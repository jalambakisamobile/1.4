FROM debian:9 as build

RUN apt update && apt-get install wget build-essential libpcre3-dev zlib1g-dev -y
RUN wget http://nginx.org/download/nginx-1.9.3.tar.gz && tar -xf nginx-1.9.3.tar.gz && \
	wget https://github.com/openresty/luajit2/archive/refs/tags/v2.1-20220309.tar.gz  && \
	wget https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.1.tar.gz && \
	wget https://github.com/openresty/lua-nginx-module/archive/refs/tags/v0.10.15.tar.gz
RUN ls *tar.gz | xargs -i tar xf {}
RUN cd /luajit2-2.1-20220309 && make && make install
WORKDIR nginx-1.9.3/
ENV LUAJIT_LIB=/usr/local/lib LUAJIT_INC=/usr/local/include/luajit-2.1
RUN ./configure --prefix=/opt/nginx \
		--with-ld-opt="-Wl,-rpath,/usr/local/lib" \
		--add-module=/ngx_devel_kit-0.3.1 \
		--add-module=/lua-nginx-module-0.10.15 \
		&& make && make install
COPY nginx.conf /opt/nginx/conf/nginx.conf

FROM debian:9

WORKDIR /opt/nginx/sbin/
COPY --from=build /opt/nginx/sbin/nginx .
RUN mkdir ../logs ../conf ../html && \
    touch ../logs/error.log &&\
    chmod +x nginx
COPY --from=build /usr/local/lib/libluajit-5.1.so.2 /usr/local/lib/
COPY --from=build /opt/nginx/conf/mime.types /opt/nginx/conf/mime.types
COPY nginx.conf /opt/nginx/conf/nginx.conf
COPY --from=build /opt/nginx/html/index.html /opt/nginx/html/index.html
CMD ["/opt/nginx/sbin/nginx", "-g", "daemon off;"]
